
let fingers;
let cap;
let mask2;
let x, y;
let song;
let mic,recorder, soundFile;
let state = 0; 


function preload(){
  fingers = createVideo('data/pc.mp4');
  mask2=loadImage('data/wall.png');
  song = loadSound('data/Listen.mp3');
   mic = new p5.AudioIn();
  
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  cap = createCapture(VIDEO);
  cap.hide();
  fingers.hide();
  imageMode(CENTER);
  fingers.play();
  song.play();
  mic.start();
  recorder = new p5.SoundRecorder();
  recorder.setInput(mic);
  soundFile = new p5.SoundFile();
}

function draw() {
  
  image(fingers,width/2, height/2, windowWidth, windowHeight);
  cap.mask(mask2);
   image(cap, x, y, mask2.width*2, mask2.height*2);

    x = width/2;
  y = height/2;
}
 
function mousePressed(){
  if (state === 0 && mic.enabled) {
 recorder.record(soundFile);
 state++;
  } else if (state === 1) {
    recorder.stop();
   state++;
   } else if (state === 2) {
     soundFile.play();
     saveSound(soundFile, 'mySound.wav');
     state++;}
     }

function windowResized() {
 resizeCanvas(windowWidth, windowHeight);
}
